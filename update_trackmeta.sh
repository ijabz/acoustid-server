#!/bin/sh
export YEAR=$1
export MONTH=$2
export DAY=$3
export LATEST=$YEAR-$MONTH-$DAY
echo "importing from $LATEST"
rm -fr $HOME/code/acoustid-server/update
mkdir $HOME/code/acoustid-server/update
cd $HOME/code/acoustid-server/update
wget https://data.acoustid.org/$YEAR/$YEAR-$MONTH/$LATEST-track_meta-update.jsonl.gz
gunzip *.gz

if test -f "$LATEST-track_meta-update.jsonl"; then
   psql jthinksearch -c "copy musicbrainz.acoustid_track_meta_json from '/home/ubuntu/code/acoustid-server/update/$LATEST-track_meta-update.jsonl'"
   #insert the new tracks only
   psql jthinksearch -c "insert into musicbrainz.acoustid_track_meta select id, track_id, meta_id, created,submission_count from acoustid_track_meta_json cross join jsonb_populate_record(null::acoustid_track_meta, data) where data::text not like '%updated%';"
   #update existing tracks when just have new ids fron existing acoustids
   psql jthinksearch -c "delete from musicbrainz.acoustid_track_meta where id in (select id from acoustid_track_meta_json cross join jsonb_populate_record(null::acoustid_track_meta, data) where data::text like '%updated%');"
   psql jthinksearch -c "insert into musicbrainz.acoustid_track_meta select id, track_id, meta_id, created,submission_count from acoustid_track_meta_json cross join jsonb_populate_record(null::acoustid_track_meta, data) where data::text like '%updated%';"
   #empty temporary json table 
   psql jthinksearch -c "truncate musicbrainz.acoustid_track_meta_json;";
fi

cd ..