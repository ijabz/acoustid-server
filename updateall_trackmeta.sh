#!/bin/bash
start_date=$(date -I -d "2019-12-01") #hard code dates to fix the issue https://github.com/acoustid/acoustid-server/issues/72
end_date=$(date -I -d "2022-10-31")   #Upto the last back up we have so can then use update_all as part of usual routine

echo "Start: $start_date"
echo "Today: $end_date"

d=$start_date                    # In case you want start_date for later?
end_d=$(date -d "$end_date" +%s) # End date in seconds

while [ $(date -d "$d" +%s) -le $end_d ]; do # Check dates in seconds
    echo ${d//-/ }               # Output the date but replace - with [space]
    ./update_trackmeta.sh ${d//-/ }
    d=$(date -I -d "$d + 1 day") # Next day
done
