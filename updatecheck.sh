#!/bin/sh
export YEAR=$1
export MONTH=$2
export DAY=$3
export LATEST=$YEAR-$MONTH-$DAY
echo "importing from $LATEST"
mkdir update
cd update
wget https://data.acoustid.org/$YEAR/$YEAR-$MONTH/$LATEST-meta-update.jsonl.gz
wget https://data.acoustid.org/$YEAR/$YEAR-$MONTH/$LATEST-track-update.jsonl.gz
wget https://data.acoustid.org/$YEAR/$YEAR-$MONTH/$LATEST-track_mbid-update.jsonl.gz
gunzip *.gz

if test -f "$LATEST-meta-update.jsonl"; then
   psql jthinksearch -c "truncate musicbrainz.acoustid_meta_json";
   psql jthinksearch -c "copy musicbrainz.acoustid_meta_json from '/home/ubuntu/code/acoustid-server/update/$LATEST-meta-update.jsonl'";
   psql jthinksearch -c "select count(*)from acoustid_meta_json cross join jsonb_populate_record(null::musicbrainz.acoustid_meta, data) where data::text not like '%updated%';"
   psql jthinksearch -c "select count(*) from acoustid_meta_json cross join jsonb_populate_record(null::musicbrainz.acoustid_meta, data) where data::text like '%updated%';"
fi

if test -f "$LATEST-track-update.jsonl"; then
   psql jthinksearch -c "truncate musicbrainz.acoustid_track_json";
   psql jthinksearch -c "copy musicbrainz.acoustid_track_json from '/home/ubuntu/code/acoustid-server/update/$LATEST-track-update.jsonl'"
   #insert the new tracks only
   psql jthinksearch -c "select count(*) from acoustid_track_json cross join jsonb_populate_record(null::acoustid_track, data) where data::text not like '%updated%';"
   #update existing tracks when just have new ids fron existing acosutids
   psql jthinksearch -c "select count(*) from acoustid_track_json cross join jsonb_populate_record(null::acoustid_track, data) where data::text like '%updated%';"
   #empty temporary json table 
fi

if test -f "$LATEST-track_mbid-update.jsonl"; then
   psql jthinksearch -c "truncate musicbrainz.acoustid_track_mbid_json";
   psql jthinksearch -c "copy musicbrainz.acoustid_track_mbid_json from '/home/ubuntu/code/acoustid-server/update/$LATEST-track_mbid-update.jsonl'";
   psql jthinksearch -c "select count(*) from acoustid_track_mbid_json cross join jsonb_populate_record(null::acoustid_track_mbid, data) where data::text not like '%updated%';"
   psql jthinksearch -c "select count(*) from acoustid_track_mbid_json cross join jsonb_populate_record(null::acoustid_track_mbid, data) where data::text like '%updated%';"
fi


cd ..
rm -fr update
