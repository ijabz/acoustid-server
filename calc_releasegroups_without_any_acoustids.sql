drop table musicbrainz.releasegroups_without_acoustids;

create table musicbrainz.releasegroups_without_acoustids (
	row_order serial primary key,
        artist_name varchar(64000),
        release_group_gid uuid,
        release_group_name varchar(64000),
        release_group_primary_type varchar(64000)
);

drop table musicbrainz.release_group_with_acoustids;

create table musicbrainz.release_group_with_acoustids (
	release_group_id int  primary key
);

-- put releasegroupid in table if any acoustids linked to releases for releasegroup have a pairing
insert into musicbrainz.release_group_with_acoustids
select distinct t5.release_group
from musicbrainz.acoustid_mbid t1
inner join musicbrainz.recording t2
on t1.mbid=t2.gid
inner join musicbrainz.track t3
on t2.id=t3.recording
inner join musicbrainz.medium t4
on t3.medium=t4.id
inner join musicbrainz.release t5
on t4.release=t5.id;

VACUUM ANALYZE musicbrainz.release_group_with_acoustids;

insert into musicbrainz.releasegroups_without_acoustids (artist_name, release_group_gid, release_group_name, release_group_primary_type)
select distinct t2.name, t1.gid, t1.name, t8.name
from musicbrainz.release_group t1
inner join release_group_primary_type t8
on t1.type=t8.id
inner join musicbrainz.artist_credit t2
on t1.artist_credit=t2.id
left join musicbrainz.release_group_with_acoustids t4
on t1.id=t4.release_group_id
where t4.release_group_id is null
and t8.name='Album' 
order by t2.name, t1.name, t8.name;
;

VACUUM ANALYZE musicbrainz.releasegroups_without_acoustids;

drop table musicbrainz.releasegroups_without_acoustids_official;

create table musicbrainz.releasegroups_without_acoustids_official (
	row_order serial primary key,
        artist_name varchar(64000),
        release_group_gid uuid,
        release_group_name varchar(64000),
        release_group_primary_type varchar(64000)
);

insert into musicbrainz.releasegroups_without_acoustids_official (artist_name, release_group_gid, release_group_name, release_group_primary_type)
select distinct t1.artist_name, t1.release_group_gid, t1.release_group_name, t1.release_group_primary_type
from musicbrainz.releasegroups_without_acoustids t1
inner join musicbrainz.release_group t2
on t1.release_group_gid=t2.gid
inner join musicbrainz.release t3
on t2.id=t3.release_group
left join release_group_secondary_type_join t4
on t2.id=t4.release_group
where t4.secondary_type is null and t3.status=1
order by t1.artist_name, t1.release_group_name;
