DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_with_trackid;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_with_trackid (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    bad_submission_count int,
    bad_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int,
    good_length int,
    good_mbid uuid,
    bad_mbid uuid
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    percent_change decimal,
    submission_count int,
    user_data varchar,
    mbid_count int
);


-- Run after we have used report.java to remove any invalid matches that have down been disabled by cjecking against live acoustid dtaabase.
DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_0;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_0 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    bad_submission_count int,
    bad_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int,
    good_length int,
    good_mbid uuid,
    bad_mbid uuid
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_1;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_1 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    bad_submission_count int,
    bad_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int,
    good_length int,
    good_mbid uuid,
    bad_mbid uuid
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_2;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_2 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    bad_submission_count int,
    bad_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int,
    good_length int,
    good_mbid uuid,
    bad_mbid uuid
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_3;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_3 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    bad_submission_count int,
    bad_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int,
    good_length int,
    good_mbid uuid,
    bad_mbid uuid
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_4;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_4 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    bad_submission_count int,
    bad_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int,
    good_length int,
    good_mbid uuid,
    bad_mbid uuid
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_5;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_5 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    bad_submission_count int,
    bad_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int,
    good_length int,
    good_mbid uuid,
    bad_mbid uuid
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_6;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_6 (
    row_order serial not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int,
    songcount int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_7;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_7 (
    row_order serial not null,
    musicbrainzid uuid NOT NULL,
    good_artistname varchar(1000),
    good_name varchar(1000),
    good_length int,
    linked_acoustid_count int,
    acoustid uuid,
    track_id int not null,
    fplength int,
    submission_count int,
    user_data varchar
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_8;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_8 (
    row_order serial not null,
    musicbrainzid uuid NOT NULL,
    good_artistname varchar(1000),
    good_name varchar(1000),
    good_length int,
    linked_acoustid_count int,
    acoustid uuid,
    track_id int not null,
    fplength int,
    submission_count int,
    user_data varchar
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_9;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_9 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_10;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_10 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_11;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_11 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_12;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_12 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_13;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_13 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_14;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_14 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);


DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_15;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_15 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);


DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_16;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_16 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);


DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_17;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_17 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);


DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_18;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_18 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_19;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_19 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_20;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_20 (
    row_order serial not null,
    track_id int not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    submission_count int,
    user_data varchar,
    mbid_count int
);
insert into musicbrainz.acoustid_invalidrecordingmatch_with_trackid (acoustid, track_id, fp_length, bad_artistname, bad_name, bad_submission_count, bad_length, good_artistname, good_name, good_submission_count, good_length,good_mbid, bad_mbid)
select t1.acoustid, t2.id, fp_length, t1.bad_artistname, t1.bad_name, t1.bad_submission_count, bad_length, t1.good_artistname, t1.good_name, t1.good_submission_count, good_length, good_mbid, bad_mbid
from musicbrainz.acoustid_invalidrecordingmatch t1
inner join musicbrainz.acoustid_track t2
on t1.acoustid=t2.gid
left join musicbrainz.acoustid_mbid_checked t3
on t1.acoustid=t3.acoustid
and t1.bad_mbid=t3.mbid
where t3.mbid is null
;


VACUUM ANALYZE musicbrainz.acoustid_invalidrecordingmatch_8;

DROP TABLE musicbrainz.acoustid_mbid_counts;

CREATE TABLE musicbrainz.acoustid_mbid_counts(
    acoustid uuid primary key,
    mbid_count int
);

--Calculate count of other mbids linked to each acoustid with bad mbid match
insert into musicbrainz.acoustid_mbid_counts (acoustid,mbid_count)
select t0.acoustid, count(*) -1
from musicbrainz.acoustid_invalidrecordingmatch_8 t0
inner join musicbrainz.acoustid_mbid t1
on t0.acoustid=t1.acoustid
left join musicbrainz.acoustid_mbid_disabled t2
on t1.acoustid=t2.acoustid
and t1.mbid=t2.mbid
where t2.acoustid is null
group by t0.acoustid;

VACUUM ANALYZE musicbrainz.acoustid_mbid_counts;

--Extend fingerprints with mbids with out of bounds recoding/track length to include no of other linked mbids, submission count and percent_difference of fp lengh/mb length
insert into musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid (acoustid, track_id, fp_length, bad_artistname, bad_name, bad_length, good_artistname, good_name, bad_mbid, percent_change, submission_count, mbid_count)
select distinct on(t1.acoustid, bad_mbid) t1.acoustid, 
t2.id, 
fp_length, 
t1.bad_artistname, 
t1.bad_name,
t1.bad_length,
t1.good_artistname, 
t1.good_name, 
bad_mbid,
abs(round((100 * (fp_length - bad_length)::decimal) / fp_length::decimal)) AS percent_change,
t4.submission_count,
t5.mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8 t1
inner join musicbrainz.acoustid_track t2
on t1.acoustid=t2.gid
inner join musicbrainz.acoustid_track_mbid t4
on t2.id=t4.track_id
and t1.bad_mbid=t4.mbid
and (t4.disabled is null or t4.disabled=false)
left join acoustid_mbid_disabled t3
on t1.acoustid=t3.acoustid
and t1.bad_mbid=t3.mbid
inner join acoustid_mbid_counts t5
on t1.acoustid=t5.acoustid
where t3.acoustid is null
order by acoustid, bad_mbid
;

VACUUM ANALYZE musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid;

--Only where same artist different song, so chances of two different songs by same artist having same acoustid is zero, so either a bad match or they actually refer to the 
--same song but the name varies, within range
insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_0 (acoustid, track_id, fp_length, bad_artistname, bad_name, bad_submission_count, bad_length, good_artistname, good_name, good_submission_count, good_length,good_mbid, bad_mbid)
select t1.acoustid, track_id, fp_length, t1.bad_artistname, t1.bad_name, t1.bad_submission_count, bad_length, t1.good_artistname, t1.good_name, t1.good_submission_count, good_length, good_mbid, bad_mbid
from musicbrainz.acoustid_invalidrecordingmatch_with_trackid t1
where good_artistname=bad_artistname
and good_submission_count >= 3
and bad_length < (fp_length + 10000)
and bad_length > (fp_length - 10000)
and 
(
	good_length < (fp_length + 10000)
	and 
	good_length > (fp_length - 10000)
)
order by good_artistname, good_name;

--Only where same artist different song, so chances of two different songs by same artist having same acoustid is zero, so either a bad match or they actually refer to the 
--same song but the name variea, outside range
insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_1 (acoustid, track_id, fp_length, bad_artistname, bad_name, bad_submission_count, bad_length, good_artistname, good_name, good_submission_count, good_length,good_mbid, bad_mbid)
select t1.acoustid, track_id, fp_length, t1.bad_artistname, t1.bad_name, t1.bad_submission_count, bad_length, t1.good_artistname, t1.good_name, t1.good_submission_count, good_length, good_mbid, bad_mbid
from musicbrainz.acoustid_invalidrecordingmatch_with_trackid t1
where good_artistname=bad_artistname
and good_submission_count >= 3
and 
(
	bad_length > (fp_length + 10000)
	and 
	bad_length < (fp_length - 10000)
)
and 
(
	good_length < (fp_length + 10000)
	and 
	good_length > (fp_length - 10000)
)
order by good_artistname, good_name;


insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_2 (acoustid, track_id, fp_length, bad_artistname, bad_name, bad_submission_count, bad_length, good_artistname, good_name, good_submission_count, good_length,good_mbid, bad_mbid)
select t1.acoustid, track_id, fp_length, t1.bad_artistname, t1.bad_name, t1.bad_submission_count, bad_length, t1.good_artistname, t1.good_name, t1.good_submission_count, good_length, good_mbid, bad_mbid
from musicbrainz.acoustid_invalidrecordingmatch_with_trackid t1
where good_submission_count >= 500
and good_artistname!=bad_artistname
and 
(
	good_length < (fp_length + 10000)
	and 
	good_length > (fp_length - 10000)
)
order by good_artistname, good_name;

--Need another table because need sorted in right order for report which uses rowno field
insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_3 (acoustid, track_id, fp_length, bad_artistname, bad_name, bad_submission_count, bad_length, good_artistname, good_name, good_submission_count, good_length,good_mbid, bad_mbid)
select t1.acoustid, track_id, fp_length, t1.bad_artistname, t1.bad_name, t1.bad_submission_count, bad_length, t1.good_artistname, t1.good_name, t1.good_submission_count, good_length,good_mbid, bad_mbid
from musicbrainz.acoustid_invalidrecordingmatch_with_trackid t1
where good_submission_count > 100
and good_submission_count < 500
and good_artistname!=bad_artistname
and 
(
	good_length < (fp_length + 10000)
	and 
	good_length > (fp_length - 10000)
)
order by good_artistname, good_name;

--Need another table because need sorted in right order for report which uses rowno field
insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_4 (acoustid, track_id, fp_length, bad_artistname, bad_name, bad_submission_count, bad_length, good_artistname, good_name, good_submission_count, good_length,good_mbid, bad_mbid)
select t1.acoustid, track_id, fp_length, t1.bad_artistname, t1.bad_name, t1.bad_submission_count, bad_length, t1.good_artistname, t1.good_name, t1.good_submission_count, good_length,good_mbid, bad_mbid
from musicbrainz.acoustid_invalidrecordingmatch_with_trackid t1
where good_submission_count >= 5
and good_submission_count < 100
and good_artistname!=bad_artistname
and 
(
	good_length < (fp_length + 10000)
	and
	good_length > (fp_length - 10000)
)
order by good_artistname, good_name;

--Need another table because need sorted in right order for report which uses rowno field
insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_5 (acoustid, track_id, fp_length, bad_artistname, bad_name, bad_submission_count, bad_length, good_artistname, good_name, good_submission_count, good_length,good_mbid, bad_mbid)
select t1.acoustid, track_id, fp_length, t1.bad_artistname, t1.bad_name, t1.bad_submission_count, bad_length, t1.good_artistname, t1.good_name, t1.good_submission_count, good_length,good_mbid, bad_mbid
from musicbrainz.acoustid_invalidrecordingmatch_with_trackid t1
where good_submission_count >= 2
and good_submission_count < 5
and good_artistname!=bad_artistname
and 
(
	good_length < (fp_length + 10000)
	and 
	good_length > (fp_length - 10000)
)
and 
(
	bad_length > (fp_length + 10000)
	or 
	bad_length < (fp_length - 10000)
)
order by good_artistname, good_name;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_6 (acoustid,good_artistname, good_name, good_submission_count, songcount)
select acoustid,good_artistname,good_name,good_submission_count, songcount
from musicbrainz.acoustid_invalidrecordingmatch_6
order by good_artistname, good_name
;

INSERT into musicbrainz.acoustid_invalidrecordingmatch_sorted_7 (musicbrainzid, good_artistname, good_name, good_length, linked_acoustid_count, acoustid, track_id, fplength,submission_count, user_data)
select musicbrainzid, good_artistname, good_name, good_length, linked_acoustid_count, acoustid, track_id, fplength,submission_count, user_data
from musicbrainz.acoustid_invalidrecordingmatch_sorted_7_temp
where 
(
        good_length > (fplength + 30000)
	or 
        good_length < (fplength - 30000)
)
order by good_artistname,good_name,fplength;

INSERT into musicbrainz.acoustid_invalidrecordingmatch_sorted_8 (musicbrainzid, good_artistname, good_name, good_length, linked_acoustid_count, acoustid, track_id, fplength,submission_count, user_data)
select musicbrainzid, good_artistname, good_name, good_length, linked_acoustid_count, acoustid, track_id, fplength,submission_count, user_data
from musicbrainz.acoustid_invalidrecordingmatch_sorted_7_temp
order by good_artistname,good_name,fplength;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_9 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 30
)
and submission_count=1
and mbid_count=0
order by bad_artistname, bad_name
;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_10 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 30
)
and submission_count=1
and mbid_count>0
order by bad_artistname, bad_name
;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_11 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 30
)
and submission_count>1
and mbid_count=0
order by bad_artistname, bad_name
;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_12 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 30
)
and submission_count>1
and mbid_count>0
order by bad_artistname, bad_name
;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_13 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data,mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 20 and percent_change <30
)
and submission_count=1
and mbid_count=0
order by bad_artistname, bad_name
;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_14 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 20 and percent_change <30
)
and submission_count=1
and mbid_count>0
order by bad_artistname, bad_name
;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_15 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data,mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 20 and percent_change <30
)
and submission_count>1
and mbid_count=0
order by bad_artistname, bad_name
;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_16 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 20 and percent_change <30
)
and submission_count>1
and mbid_count>0
order by bad_artistname, bad_name
;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_17 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 10 and percent_change <20
)
and submission_count=1
and mbid_count=0
order by bad_artistname, bad_name
;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_18 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 10 and percent_change <20
)
and submission_count=1
and mbid_count>0
order by bad_artistname, bad_name
;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_19 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 10 and percent_change <20
)
and submission_count>1
and mbid_count=0
order by bad_artistname, bad_name
;

insert into musicbrainz.acoustid_invalidrecordingmatch_sorted_20 (acoustid, track_id, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count)
select acoustid, track_id, fp_length, good_artistname,good_name,bad_mbid, bad_length, bad_artistname, bad_name, submission_count, user_data, mbid_count
from musicbrainz.acoustid_invalidrecordingmatch_8_with_trackid
where 
(
	percent_change >= 10 and percent_change <20
)
and submission_count>1
and mbid_count>0
order by bad_artistname, bad_name
;