#!/bin/bash
psql jthinksearch < fix_bad_data.sql
export YEAR=$1
export MONTH=$2
export DAY=$3
export VERSION=$YEAR-$MONTH-$DAY
set -x
rm -fr export
mkdir export
cd export
psql jthinksearch -c "COPY musicbrainz.acoustid_track_fingerprint TO '/tmp/acoustid_track_fingerprint_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_track TO '/tmp/acoustid_track_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_meta TO '/tmp/acoustid_meta_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_track_meta TO '/tmp/acoustid_track_meta_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_track_mbid TO '/tmp/acoustid_track_mbid_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_fingerprint TO '/tmp/acoustid_fingerprint_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_mbid_disabled TO '/tmp/acoustid_mbid_disabled_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_mbid_checked TO '/tmp/acoustid_mbid_checked_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
sudo mv /tmp/*.csv /home/ubuntu/code/acoustid-server/export
cd /home/ubuntu/code/acoustid-server/export
gzip *.csv
s3cmd put acoustid_track_fingerprint_$VERSION.csv.gz s3://albunack;
s3cmd put acoustid_track_$VERSION.csv.gz s3://albunack;
s3cmd put acoustid_meta_$VERSION.csv.gz s3://albunack;
s3cmd put acoustid_track_meta_$VERSION.csv.gz s3://albunack;
s3cmd put acoustid_track_mbid_$VERSION.csv.gz s3://albunack;
s3cmd put acoustid_fingerprint_$VERSION.csv.gz s3://albunack;
s3cmd put acoustid_mbid_disabled_$VERSION.csv.gz s3://albunack;
s3cmd put acoustid_mbid_checked_$VERSION.csv.gz s3://albunack;
