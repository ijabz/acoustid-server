
-- Albums that seem to match existing musicbrainz albums but we dont have acoistids for the links
-- but we could add them in
DROP TABLE musicbrainz.acoustid_complete_not_matched_album_temp2;

CREATE TABLE musicbrainz.acoustid_complete_not_matched_album_temp2
(
    id int primary key,
    album_artist varchar,
    album_artist_gid uuid,
    album varchar,
    track_total int,
    musicbrainz_match int,
    year varchar
);

insert into musicbrainz.acoustid_complete_not_matched_album_temp2 (id, album_artist, album_artist_gid,album, track_total, musicbrainz_match, year)
select distinct on (t1.id) t1.id,
regexp_replace(t1.album_artist,'^[\t\n\r ]*','','g'),
t4.artistid, 
regexp_replace(t1.album, '^[\t\n\r ]*','','g'),
t1.track_total,
t1.musicbrainz_match,
t1.year
from musicbrainz.acoustid_complete_album t1
inner join musicbrainz.artist t3
on lower(t1.album_artist)=lower(t3.name)
left join musicbrainz.unique_artist t4
on lower(t1.album_artist)=lower(t4.name)
left join  musicbrainz.acoustid_complete_matched_album t2
on t1.id=t2.id 
where t2.id is null
and t1.album_artist!='!!!'
order by t1.id
;

DROP TABLE musicbrainz.acoustid_complete_not_matched_album_temp;

CREATE TABLE musicbrainz.acoustid_complete_not_matched_album_temp
(
    id int primary key,
    album_artist varchar,
    album_artist_gid uuid,
    album varchar,
    track_total int,
    musicbrainz_match int,
    year varchar
);

insert into musicbrainz.acoustid_complete_not_matched_album_temp
select * 
from musicbrainz.acoustid_complete_not_matched_album_temp2
where lower(album) not like '%cd1%'
and lower(album) not like   '%cd2%'
and lower(album) not like   '%cd3%'
and lower(album) not like   '%cd4%'
and lower(album) not like   '%cd5%'
and lower(album) not like   '%cd6%'
and lower(album) not like   '%cd7%'
and lower(album) not like   '%cd8%'
and lower(album) not like   '%cd9%'
and lower(album) not like   '%cd %'
and lower(album) not like   '%disc %'
and lower(album) not like   '%disk %'
and lower(album) not like   '%disc1%'
and lower(album) not like   '%disc2%'
and lower(album) not like   '%disc3%'
and lower(album) not like   '%disc4%'
and lower(album) not like   '%disc5%'
and lower(album) not like   '%disc6%'
and lower(album) not like   '%disc7%'
and lower(album) not like   '%disc8%'
and lower(album) not like   '%disc9%'
and lower(album) not like   '%disk1%'
and lower(album) not like   '%disk2%'
and lower(album) not like   '%disk3%'
and lower(album) not like   '%disk4%'
and lower(album) not like   '%disk5%'
and lower(album) not like   '%disk6%'
and lower(album) not like   '%disk7%'
and lower(album) not like   '%disk8%'
and lower(album) not like   '%disk9%'
and lower(album) not like   '%vol %'
and lower(album) not like   '%volume %'
;
--Release Count to try and get most popular artists
DROP TABLE musicbrainz.release_count;

CREATE TABLE musicbrainz.release_count
(
    artist_name varchar primary key,
    release_count int
);

INSERT INTO musicbrainz.release_count
SELECT lower(t4.name), count(*)
FROM musicbrainz.release t1
INNER JOIN musicbrainz.artist_credit t2
ON t1.artist_credit=t2.id
INNER JOIN musicbrainz.artist_credit_name t3
ON t2.id=t3.artist_credit
AND t3.position=0
INNER JOIN musicbrainz.artist t4
ON t3.artist=t4.id
GROUP by lower(t4.name);

VACUUM ANALYZE musicbrainz.release_count;

DROP TABLE musicbrainz.acoustid_complete_not_matched_album;

CREATE TABLE musicbrainz.acoustid_complete_not_matched_album 
(
    row_order serial not null,
    id int primary key,
    album_artist varchar,
    album_artist_gid uuid,
    album varchar,
    track_total int,
    musicbrainz_match int,
    year varchar,
    release_count int
);

insert into musicbrainz.acoustid_complete_not_matched_album (id, album_artist, album_artist_gid,album, track_total, musicbrainz_match, year, release_count)
select t1.*,t2.release_count
FROM 
musicbrainz.acoustid_complete_not_matched_album_temp t1
INNER JOIN musicbrainz.release_count t2
ON lower(t1.album_artist)=lower(t2.artist_name)
WHERE t2.release_count > 100
order by t1.album_artist, t1.album
;

DROP TABLE musicbrainz.acoustid_complete_not_matched_album2;

CREATE TABLE musicbrainz.acoustid_complete_not_matched_album2 
(
    row_order serial not null,
    id int primary key,
    album_artist varchar,
    album_artist_gid uuid,
    album varchar,
    track_total int,
    musicbrainz_match int,
    year varchar,
    release_count int
);

insert into musicbrainz.acoustid_complete_not_matched_album2 (id, album_artist, album_artist_gid,album, track_total, musicbrainz_match, year, release_count)
select t1.*,t2.release_count
FROM 
musicbrainz.acoustid_complete_not_matched_album_temp t1
INNER JOIN musicbrainz.release_count t2
ON lower(t1.album_artist)=lower(t2.artist_name)
WHERE t2.release_count > 1 and t2.release_count < 101
order by t1.album_artist, t1.album
;

DROP TABLE musicbrainz.acoustid_complete_not_matched_album3;

CREATE TABLE musicbrainz.acoustid_complete_not_matched_album3 
(
    row_order serial not null,
    id int primary key,
    album_artist varchar,
    album_artist_gid uuid,
    album varchar,
    track_total int,
    musicbrainz_match int,
    year varchar,
    release_count int
);

insert into musicbrainz.acoustid_complete_not_matched_album3 (id, album_artist, album_artist_gid,album, track_total, musicbrainz_match, year, release_count)
select t1.*,t2.release_count
FROM 
musicbrainz.acoustid_complete_not_matched_album_temp t1
INNER JOIN musicbrainz.release_count t2
ON lower(t1.album_artist)=lower(t2.artist_name)
WHERE t2.release_count < 2
order by t1.album_artist, t1.album
;
