CREATE EXTENSION fuzzystrmatch;

DROP TABLE musicbrainz.acoustid_names_staging;

CREATE TABLE musicbrainz.acoustid_names_staging (
    acoustid uuid NOT NULL,
    artistname varchar(64000),
    name varchar(64000),
    mbid uuid,
    length int,
    submission_count int,
    fp_length int
);

DROP TABLE musicbrainz.acoustid_names_staging_2;

CREATE TABLE musicbrainz.acoustid_names_staging_2 (
    acoustid uuid NOT NULL,
    artistname varchar(64000),
    mbid uuid,
    length int,
    name varchar(64000)
);

DROP TABLE musicbrainz.acoustid_names;

CREATE TABLE musicbrainz.acoustid_names (
    acoustid uuid NOT NULL,
    artistname varchar(64000),
    name varchar(64000),
    mbid uuid,
    length int,
    submission_count int,
    fp_length int
);

DROP TABLE musicbrainz.acoustid_multiplenames;

CREATE TABLE musicbrainz.acoustid_multiplenames (
    acoustid uuid NOT NULL,
    artistname varchar(64000),
    name varchar(64000),
    mbid uuid,
    length int,
    submission_count int,
    fp_length int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_staging;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_staging (
    row_order serial not null,
    acoustid uuid NOT NULL,
    fp_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    bad_mbid uuid,
    bad_submission_count int,
    bad_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_mbid uuid,
    good_submission_count int,
    good_length int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch (
    row_order serial not null,
    acoustid uuid NOT NULL,
    fp_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    bad_mbid uuid,
    bad_submission_count int,
    bad_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_mbid uuid,
    good_submission_count int,
    good_length int
);

DROP TABLE musicbrainz.acoustid_not_unique;

CREATE TABLE musicbrainz.acoustid_not_unique
(
    acoustid uuid NOT NULL,
    PRIMARY KEY (acoustid)
);


DROP TABLE musicbrainz.acoustid_fp_length;

CREATE TABLE musicbrainz.acoustid_fp_length
(
    acoustid uuid NOT NULL,
    fp_length int,
    PRIMARY KEY (acoustid)
);

-- Clear out and recreate to take disabled tabvle into account
DROP table musicbrainz.acoustid_mbid;

CREATE TABLE musicbrainz.acoustid_mbid (
    id serial primary key,
    acoustid uuid NOT NULL,
    mbid uuid NOT NULL
);

CREATE INDEX acoustid_mbid_index ON musicbrainz.acoustid_mbid (mbid);

-- Create table of acoutid/mbids (excluding anyones we have disabled)
insert into acoustid_mbid(acoustid, mbid) 
select t1.gid,t2.mbid 
from acoustid_track t1 
inner join acoustid_track_mbid t2 
on t1.id=t2.track_id 
left join acoustid_mbid_disabled t3
on t1.gid=t3.acoustid
and t2.mbid=t3.mbid
where t2.disabled=false 
and t3.acoustid is null
;
 
-- Only interested in Acoustids linked to at least two mbids because we are looking for duplicates
insert into acoustid_not_unique 
select t1.acoustid
from acoustid_mbid t1
group by t1.acoustid
having count(*) >1;

VACUUM ANALYZE musicbrainz.acoustid_not_unique;

-- Now for each of these acoustids we get the fingerprint length
insert into acoustid_fp_length
select t1.acoustid, (min(t4.length) + 3) * 1000 
from acoustid_not_unique t1
inner join acoustid_track t2
on t1.acoustid=t2.gid
left join acoustid_track_fingerprint t3
on t2.id=t3.track_id
left join acoustid_fingerprint t4
on t4.id=t3.fingerprint_id
group by t1.acoustid;

VACUUM ANALYZE musicbrainz.acoustid_fp_length;

-- So for this sublist we then find associated mbid, artist name, track length and workName or trackName
insert into musicbrainz.acoustid_names_staging_2
select distinct on (t1.acoustid,t2.name, t2.gid) t1.acoustid,t10.name, t2.gid, t2.length, coalesce(t6.name,t2.name)
from acoustid_not_unique t1
inner join  acoustid_mbid t11
on t1.acoustid=t11.acoustid
inner join musicbrainz.recording t2
on t11.mbid=t2.gid
inner join musicbrainz.artist_credit t10
on t2.artist_credit=t10.id
left join l_recording_work t5
on t2.id=t5.entity0
inner join work t6
on t5.entity1=t6.id;

VACUUM ANALYZE musicbrainz.acoustid_names_staging_2;

-- Add in submission counts and fingerprint length
insert into musicbrainz.acoustid_names_staging (acoustid, artistname, name, mbid, length, submission_count, fp_length)
select distinct t1.acoustid,t1.artistname, t1.name, cast (max(t1.mbid::text) as uuid), max(t1.length), sum(t4.submission_count), min(t5.fp_length)
from acoustid_names_staging_2 t1
inner join acoustid_track t3
on t1.acoustid=t3.gid
inner join acoustid_track_mbid t4
on t3.id=t4.track_id
and t1.mbid=t4.mbid
inner join acoustid_fp_length t5
on t1.acoustid=t5.acoustid
group by t1.acoustid, t1.artistname, t1.name
;

-- Tidy up data
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' & ',' and ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' ii ',' 2 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' iii ',' 3 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' iv ',' 4 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' v ',' 5 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' vi ',' 6 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' vii ',' 7 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' viii ',' 8 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' xi ',' 9 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' two ',' 2 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' three ',' 3 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' four ',' 4 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' five ',' 5 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' six ',' 6 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' seven ',' 7 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' eight ',' 8 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' nine ',' 9 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' ten ',' 10 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' eleven ',' 11 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' twelve ',' 12 ');
update musicbrainz.acoustid_names_staging set name=REPLACE(name,' thirteen ',' 13 ');

update musicbrainz.acoustid_names_staging set name=regexp_replace(name,'^(two )(.*)','2 \2 ');
update musicbrainz.acoustid_names_staging set name=regexp_replace(name,'^(three )(.*)','3 \2 ');
update musicbrainz.acoustid_names_staging set name=regexp_replace(name,'^(four )(.*)','4 \2 ');
update musicbrainz.acoustid_names_staging set name=regexp_replace(name,'^(five )(.*)','5 \2 ');
update musicbrainz.acoustid_names_staging set name=regexp_replace(name,'^(six )(.*)','6 \2 ');
update musicbrainz.acoustid_names_staging set name=regexp_replace(name,'^(seven )(.*)','7 \2 ');
update musicbrainz.acoustid_names_staging set name=regexp_replace(name,'^(eight )(.*)','8 \2 ');
update musicbrainz.acoustid_names_staging set name=regexp_replace(name,'^(nine )(.*)','9 \2 ');
update musicbrainz.acoustid_names_staging set name=regexp_replace(name,'^(ten )(.*)','10 \2 ');
update musicbrainz.acoustid_names_staging set name=regexp_replace(name,'^(eleven )(.*)','11 \2 ');

-- Now we remove brackets and do any further grouping/summing up
insert into musicbrainz.acoustid_names
select acoustid, artistname, regexp_replace(trim(lower(name)),'^(.*)\(.*?\)','\1'), cast(max(mbid::text) as uuid), max(length), sum(submission_count), max(fp_length)
from musicbrainz.acoustid_names_staging
group by acoustid, artistname, regexp_replace(trim(lower(name)),'^(.*)\(.*?\)','\1')
;

VACUUM ANALYZE musicbrainz.acoustid_names;

--Filter so only contains acoustids where they link to more than one recording with a different simplified name
--and after running soundex on the name should still be more than one name for the acoustid, i.e soundex doesnt
--resolves all the names for the acoustid to one value
insert into musicbrainz.acoustid_multiplenames
select t1.acoustid, t1.artistname, t1.name, cast(max(mbid::text) as uuid), max(length), sum(submission_count), max(t1.fp_length)
from musicbrainz.acoustid_names t1
inner join
(
select acoustid from musicbrainz.acoustid_names
group by acoustid
having count(*) >1
) t2
on t1.acoustid=t2.acoustid
inner join
(
select distinct acoustid from musicbrainz.acoustid_names
group by acoustid, soundex(name)
having count(*) =1
) t3
on t2.acoustid=t3.acoustid
group by t1.acoustid, t1.artistname, t1.name
order by t1.acoustid, t1.artistname,sum(submission_count);

VACUUM ANALYZE musicbrainz.acoustid_multiplenames;

-- Now for each acoustid that has multiple mbnames linked to it we put the one with lowest source count
--into own table, but only if source is 1 or less and there ids a good match with a submissionc count more than 1
insert into musicbrainz.acoustid_invalidrecordingmatch_staging (acoustid, fp_length, bad_artistname, bad_name, bad_mbid, bad_submission_count, bad_length, good_artistname, good_name, good_mbid, good_submission_count, good_length)
select distinct on (t1.acoustid)  t1.acoustid, t1.fp_length, t1.artistname, t1.name, t1.mbid, t1.submission_count, t1.length, t2.artistname, t2.name, t2.mbid, t2.submission_count, t2.length
from musicbrainz.acoustid_multiplenames t1
inner join musicbrainz.acoustid_multiplenames t2
on t1.acoustid=t2.acoustid
and t1.name!=t2.name
where t1.submission_count =1
and t2.submission_count > 1
order by t1.acoustid ASC, t2.submission_count DESC;

insert into musicbrainz.acoustid_invalidrecordingmatch (acoustid, fp_length, bad_artistname, bad_name, bad_mbid, bad_submission_count, bad_length, good_artistname, good_name, good_mbid, good_submission_count,good_length)
select acoustid, fp_length, bad_artistname, bad_name, bad_mbid, bad_submission_count, bad_length, good_artistname, good_name, good_mbid, good_submission_count, good_length
from musicbrainz.acoustid_invalidrecordingmatch_staging
order by good_artistname, good_name;

VACUUM ANALYZE musicbrainz.acoustid_invalidrecordingmatch;

update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' & ',' and ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' & ',' and ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' ii ',' 2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' ii ',' 2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' iii ',' 3 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' iii ',' 3 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' iv ',' 4 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' iv ',' 4 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' v ',' 5 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' v ',' 5 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' vi ',' 6 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' vi ',' 6 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' vii ',' 7 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' vii ',' 7 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' viii ',' 8 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' viii ',' 8 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' ix ',' 9 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' ix ',' 9 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' two ',' 2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' two ',' 2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' three ',' 3 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' three ',' 3 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' four ',' 4 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' four ',' 4 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' five ',' 5 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' five  ',' 5 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' six ',' 6 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' six  ',' 6 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' seven ',' 7 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' seven ',' 7 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' eight ',' 8 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' eight ',' 8 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' nine ',' 9 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' nine ',' 9 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' ten ',' 10 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' ten ',' 10 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' eleven ',' 11 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' eleven ',' 11 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' twelve ',' 12 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' twelve ',' 12 ');

update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(two )(.*)','2 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(two )(.*)','2 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(three )(.*)','3 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(three )(.*)','3 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(four )(.*)','4 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(four )(.*)','4 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(five )(.*)','5 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(five )(.*)','5 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(six )(.*)','6 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(six )(.*)','6 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(seven )(.*)','7 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(seven )(.*)','7 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(eight )(.*)','8 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(eight )(.*)','8 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(nine )(.*)','9 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(nine )(.*)','9 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(ten )(.*)','10 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(ten )(.*)','10 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(eleven )(.*)','11 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(eleven )(.*)','11 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(twelve )(.*)','12 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(twelve )(.*)','12 \2 ');

--Filter out matches where by the goodname is actually quite simailr to the badname
delete from musicbrainz.acoustid_invalidrecordingmatch t1
where t1.bad_name like t1.good_name || '%';

delete from musicbrainz.acoustid_invalidrecordingmatch t1
where t1.good_name like t1.bad_name || '%';

delete from musicbrainz.acoustid_invalidrecordingmatch t1
where t1.bad_name like '%' || t1.good_name;

delete from musicbrainz.acoustid_invalidrecordingmatch t1
where t1.good_name like '%' || t1.bad_name;

delete from musicbrainz.acoustid_invalidrecordingmatch 
where LEVENSHTEIN(substring(good_name,1,20),substring(bad_name,1,20)) < 4;

--Find songs linked to many sources
DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_6_staging;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_6_staging (
    row_order serial not null,
    acoustid uuid NOT NULL,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int,
    songcount int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_6;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_6 (
    row_order serial not null,
    acoustid uuid NOT NULL,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int,
    songcount int
);

insert into musicbrainz.acoustid_invalidrecordingmatch_6_staging (acoustid,good_artistname, good_name, good_submission_count, songcount)
select distinct on (t2.acoustid)  t2.acoustid, t2.artistname, t2.name, t2.submission_count, t1.songcount
from
(
select acoustid, count(*) as songcount
from musicbrainz.acoustid_names
group by acoustid
having count(*) >=5
) t1
inner join musicbrainz.acoustid_names t2
on t1.acoustid=t2.acoustid
order by t2.acoustid,t2.submission_count desc;

insert into musicbrainz.acoustid_invalidrecordingmatch_6 (acoustid,good_artistname, good_name, good_submission_count, songcount)
select t1.acoustid, t1.good_artistname, t1.good_name, t1.good_submission_count, t1.songcount
from musicbrainz.acoustid_invalidrecordingmatch_6_staging t1
order by t1.good_artistname, t1.good_name;

-- Mb recordings linked to 50 different acoustids
DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_7_summary;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_7_summary (
    row_order serial not null,
    musicbrainzid uuid NOT NULL,
    good_artistname varchar(1000),
    good_name varchar(1000),
    good_length int,
    linked_acoustid_count int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_7;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_7 (
    row_order serial not null,
    musicbrainzid uuid NOT NULL,
    good_artistname varchar(1000),
    good_name varchar(1000),
    good_length int,
    linked_acoustid_count int,
    acoustid uuid,
    fplength int
);

INSERT into musicbrainz.acoustid_invalidrecordingmatch_sorted_7_summary (musicbrainzid, good_artistname, good_name, good_length, linked_acoustid_count)
select t1.mbid , t3.name ,t2.name , t2.length, count(*)
from musicbrainz.acoustid_mbid t1
inner join musicbrainz.recording t2
on t1.mbid = t2.gid
inner join musicbrainz.artist_credit t3
on t3.id = t2.artist_credit
left join musicbrainz.acoustid_mbid_disabled t10
on t1.mbid=t10.mbid
and t1.acoustid=t10.acoustid
where t10.mbid is null
group by t1.mbid, t2.name, t3.name, t2.length
having count(*) > 50
order by t3.name, t2.name
;

INSERT into musicbrainz.acoustid_invalidrecordingmatch_7 (musicbrainzid, good_artistname, good_name, good_length, linked_acoustid_count, acoustid, fplength)
select distinct on (t1.musicbrainzid,t2.acoustid) t1.musicbrainzid, t1.good_artistname, t1.good_name, t1.good_length, t1.linked_acoustid_count, t2.acoustid, t5.length * 1000
from musicbrainz.acoustid_invalidrecordingmatch_sorted_7_summary t1
inner join musicbrainz.acoustid_mbid t2
on t1.musicbrainzid = t2.mbid
inner join musicbrainz.acoustid_track t3
on t2.acoustid=t3.gid 
inner join acoustid_track_fingerprint t4
on t3.id=t4.track_id
inner join acoustid_fingerprint t5
on t4.fingerprint_id=t5.id
;

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_7_temp;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted_7_temp (
    row_order serial not null,
    musicbrainzid uuid NOT NULL,
    good_artistname varchar(1000),
    good_name varchar(1000),
    good_length int,
    linked_acoustid_count int,
    acoustid uuid,
    track_id int not null,
    fplength int,
    submission_count int,
    user_data varchar
);

INSERT into musicbrainz.acoustid_invalidrecordingmatch_sorted_7_temp (musicbrainzid, good_artistname, good_name, good_length, linked_acoustid_count, acoustid, track_id, fplength,submission_count, user_data)
select distinct on (t1.musicbrainzid, t1.acoustid)t1.musicbrainzid, t1.good_artistname, t1.good_name, t1.good_length, t1.linked_acoustid_count, t1.acoustid, t3.id, t1.fplength , t4.submission_count, t6.artist || '/' || t6.album || '/' || t6.track
from musicbrainz.acoustid_invalidrecordingmatch_7 t1
inner join musicbrainz.acoustid_track t3
on t1.acoustid=t3.gid
inner join musicbrainz.acoustid_track_mbid t4
on t3.id=t4.track_id
and t1.musicbrainzid=t4.mbid
left join musicbrainz.acoustid_track_meta t5
on t3.id=t5.track_id
left join musicbrainz.acoustid_meta t6
on t5.meta_id=t6.id
left join musicbrainz.acoustid_mbid_disabled t2
on t1.musicbrainzid=t2.mbid
and t1.acoustid=t2.acoustid
where t2.mbid is null
order by t1.musicbrainzid, t1.acoustid;

DROP TABLE musicbrainz.acoustid_length_doesnt_match;

CREATE TABLE musicbrainz.acoustid_length_doesnt_match(
    acoustid uuid NOT NULL,
    fp_length int,
    mbid uuid,
    length int
);

DROP TABLE musicbrainz.acoustid_fp_length_all;

CREATE TABLE musicbrainz.acoustid_fp_length_all
(
    acoustid uuid NOT NULL,
    fp_length int,
    PRIMARY KEY (acoustid)
);

insert into acoustid_fp_length_all
select distinct t1.acoustid, (min(t4.length) + 3) * 1000 
from acoustid_mbid t1
inner join acoustid_track t2
on t1.acoustid=t2.gid
left join acoustid_track_fingerprint t3
on t2.id=t3.track_id
left join acoustid_fingerprint t4
on t4.id=t3.fingerprint_id
group by t1.acoustid;

-- Find pairings where fingerprint length and musicbrainz length out by more than 30
-- ignore bad matches already coverred by other reports
insert into musicbrainz.acoustid_length_doesnt_match
select t1.acoustid, t1.fp_length, t2.mbid, t3.length
from musicbrainz.acoustid_fp_length_all t1
inner join musicbrainz.acoustid_mbid t2
on t1.acoustid=t2.acoustid
inner join musicbrainz.recording t3
on t2.mbid=t3.gid
left join musicbrainz.acoustid_invalidrecordingmatch t4
on t1.acoustid=t4.acoustid
and t2.mbid=t4.bad_mbid
where 
(
	t3.length > (t1.fp_length + 30000)
	or t3.length < (t1.fp_length - 30000)
)
and t4.acoustid is null
;

CREATE INDEX acoustid_length_doesnt_match_acoustid_index ON musicbrainz.acoustid_length_doesnt_match (acoustid);
CREATE INDEX acoustid_length_doesnt_match_mbid_index ON musicbrainz.acoustid_length_doesnt_match (mbid);
CREATE INDEX acoustid_length_doesnt_match_acoustid_mbid_index ON musicbrainz.acoustid_length_doesnt_match (acoustid,mbid);

VACUUM ANALYZE musicbrainz.acoustid_length_doesnt_match;

DROP TABLE musicbrainz.acoustid_tracklength_does_match;

-- Create table to store fingerprints that domatch recording linked to track with valid track length, even though there was no recording with valid link
CREATE TABLE musicbrainz.acoustid_tracklength_does_match(
    acoustid uuid NOT NULL,
    fp_length int,
    mbid uuid,
    length int
);


-- Find pairings from the table of fingerprint length and musicbrainz length out by more than 30 but where there is actuaklly a good track match so we dont want to flag as bad pairing
insert into musicbrainz.acoustid_tracklength_does_match
select t1.acoustid, t1.fp_length, t1.mbid, t1.length
from musicbrainz.acoustid_length_doesnt_match t1
inner join musicbrainz.recording t2
on t1.mbid=t2.gid
inner join musicbrainz.track t3
on t3.recording=t2.id
where 
(
	t3.length < (t1.fp_length + 30000)
	and t3.length > (t1.fp_length - 30000)
)
;

VACUUM ANALYZE musicbrainz.acoustid_tracklength_does_match;


DROP TABLE musicbrainz.acoustid_length_doesnt_match2;

CREATE TABLE musicbrainz.acoustid_length_doesnt_match2(
    acoustid uuid NOT NULL,
    fp_length int,
    mbid uuid,
    length int
);

-- Now Filter the table of fingerpints with no matching recordings to remove those that do have matching track length
insert into musicbrainz.acoustid_length_doesnt_match2
select t1.acoustid, t1.fp_length, t1.mbid, t1.length
from musicbrainz.acoustid_length_doesnt_match t1
left join musicbrainz.acoustid_tracklength_does_match t2
on  t1.acoustid=t2.acoustid
and t1.mbid=t2.mbid
where t2.acoustid is null;

VACUUM ANALYZE musicbrainz.acoustid_length_doesnt_match2;

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_8;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_8 (
    row_order serial not null,
    acoustid uuid NOT NULL,
    fp_length int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    bad_mbid uuid,
    bad_length int,
    bad_artistname varchar(64000),
    bad_name varchar(64000)
);

insert into musicbrainz.acoustid_invalidrecordingmatch_8 (acoustid, fp_length, good_artistname, good_name, bad_mbid, bad_length, bad_artistname, bad_name)
select distinct on (t1.acoustid, t1.mbid) t1.acoustid, t1.fp_length,'','', t1.mbid, t1.length, t5.name, t3.name 
from musicbrainz.acoustid_length_doesnt_match2 t1
inner join musicbrainz.recording t3
on t1.mbid=t3.gid
inner join musicbrainz.artist_credit t5
on t3.artist_credit=t5.id
;
