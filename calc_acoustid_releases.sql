
-- Find the best metadata irow fRorro each trafck 
DROP TABLE musicbrainz.acoustid_track_best_metadata;

CREATE TABLE musicbrainz.acoustid_track_best_metadata 
(
    id serial primary key,
    album_artist      varchar,
    artist     varchar,
    album      varchar,
    track_no   varchar,
    disc_no    varchar,
    track      varchar,
    year   varchar,
    meta_id  int
);

DROP TABLE musicbrainz.acoustid_meta_mbid;

CREATE TABLE musicbrainz.acoustid_meta_mbid 
(
    meta_id int
);


-- Group tracks into albums
DROP TABLE musicbrainz.acoustid_album;

CREATE TABLE musicbrainz.acoustid_album 
(
    id serial primary key,
    album_artist varchar,
    album varchar,
    track_total int, 
    year varchar,
    musicbrainz_match int
);

-- Group tracks into albums
DROP TABLE musicbrainz.acoustid_complete_album;

CREATE TABLE musicbrainz.acoustid_complete_album 
(
    id serial primary key,
    album_artist varchar,
    album varchar,
    track_total int, 
    year varchar,
    musicbrainz_match int
);

-- Get metadata for the album
DROP TABLE musicbrainz.acoustid_track_album;

CREATE TABLE musicbrainz.acoustid_track_album 
(
    id serial primary key,
    meta_id int,
    album_id int,
    artist     varchar,
    track      varchar,
    album      varchar,
    album_artist      varchar,
    disc_no    varchar,
    track_no   varchar,
    year   varchar
);

--Tidy up data, but also for testing alllows us to restrict to one albumArtist
insert into musicbrainz.acoustid_track_best_metadata (album_artist, artist, album, track_no, disc_no, track, year, meta_id)
select distinct on (trim(t2.album_artist), trim(t2.album), t2.track_no::int) trim(t2.album_artist), trim(t2.artist), trim(t2.album), t2.track_no, t2.disc_no, trim(t2.track), t2.year, t2.id
from  musicbrainz.acoustid_track_meta t1
inner join musicbrainz.acoustid_meta t2
on t1.meta_id=t2.id
and t2.artist is not null
and t2.album is not null
and t2.track is not null
and t2.track_no is not null
and t2.track not like 'Track%'
and t2.track not like '1%'
and t2.track not like '0%'
order by trim(t2.album_artist), trim(t2.album), t2.track_no::int;

VACUUM ANALYZE musicbrainz.acoustid_track_best_metadata;

CREATE INDEX acoustid_track_best_metadata_album_idx ON acoustid_track_best_metadata (album);
CREATE INDEX acoustid_track_best_metadata_album_artist_idx ON acoustid_track_best_metadata (album_artist);

-- Meta records that have associated acoustid track with a valid link to mbrecording
insert into musicbrainz.acoustid_meta_mbid (meta_id)
select distinct t1.meta_id
from  musicbrainz.acoustid_track_best_metadata t1
inner join musicbrainz.acoustid_track_meta t2
on t1.meta_id=t2.meta_id
inner join musicbrainz.acoustid_track t3
on t2.track_id=t3.id
inner join acoustid_mbid t4
on t3.gid=t4.acoustid
left join acoustid_mbid_disabled t5
on t4.acoustid=t5.acoustid
and t4.mbid=t5.mbid
where t5.acoustid is null
;

-- Create AlbumArtist, Album grouping
-- only want albums that are more than one tracks and not all tracks already linked to mb recordings
insert into musicbrainz.acoustid_album (album_artist, album, track_total, year, musicbrainz_match)
select t2.album_artist, t2.album, count(*), min(t2.year), count(t3.meta_id)
from musicbrainz.acoustid_track_best_metadata t2
left join musicbrainz.acoustid_meta_mbid t3
on t2.meta_id=t3.meta_id
group by t2.album_artist, t2.album
having count(*)>5 and count(t3.meta_id) < 4 and count(t3.meta_id) < count(*);

VACUUM ANALYZE musicbrainz.acoustid_album;

-- Now find tracks for albums
insert into musicbrainz.acoustid_track_album (meta_id, album_id, artist, track, album, album_artist, disc_no, track_no, year)
select distinct on (t1.track_no::int, t3.id) t1.meta_id, t3.id,t1.artist,t1.track,t1.album, t1.album_artist, t1.disc_no , t1.track_no, t1.year
from musicbrainz.acoustid_track_best_metadata t1
inner join  musicbrainz.acoustid_album t3
on t1.album_artist=t3.album_artist
and t1.album=t3.album
order by t3.id, t1.track_no::int;
;

VACUUM ANALYZE musicbrainz.acoustid_track_album;

CREATE INDEX acoustid_track_album_albumid_idx ON acoustid_track_album (album_id);
CREATE INDEX acoustid_track_album_artist_idx ON acoustid_track_album (artist);
CREATE INDEX acoustid_track_album_metaid_idx ON acoustid_track_album (meta_id);

-- Show only complete albums where track_total equals max track_no
insert into musicbrainz.acoustid_complete_album
select distinct t1.* 
FROM musicbrainz.acoustid_album t1
INNER JOIN 
(
select t2.album,
t2.album_artist,
max(t2.track_no::int) as max_track
from musicbrainz.acoustid_track_album t2
group by t2.album_artist, t2.album
) as t3
on  t1.album_artist=t3.album_artist
and t1.album=t3.album
and t1.track_total=t3.max_track
;

VACUUM ANALYZE musicbrainz.acoustid_complete_album;

CREATE INDEX acoustid_complete_album_album_artist_idx ON acoustid_complete_album (album_artist);
CREATE INDEX acoustid_complete_album_albumid_album_idx ON acoustid_complete_album (album);

select count(*) from musicbrainz.acoustid_complete_album
;

