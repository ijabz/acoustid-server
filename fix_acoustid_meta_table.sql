--have to fix this before we can create an index on it
--update musicbrainz.acoustid_meta set album=substr(album,0,1000) where char_length(album)>1000;
--update musicbrainz.acoustid_meta set track=substr(track,0,1000) where char_length(track)>1000;
CREATE INDEX acoustid_meta_album_index ON musicbrainz.acoustid_meta (album);
CREATE INDEX acoustid_meta_artist_index ON musicbrainz.acoustid_meta (artist);
CREATE INDEX acoustid_meta_album_artist_index ON musicbrainz.acoustid_meta (album_artist);
CREATE INDEX acoustid_meta_track_index ON musicbrainz.acoustid_meta (track);
CREATE INDEX acoustid_meta_year_index ON musicbrainz.acoustid_meta (year);
CREATE INDEX acoustid_meta_track_no_index ON musicbrainz.acoustid_meta (track_no);
CREATE INDEX acoustid_meta_disc_no_index ON musicbrainz.acoustid_meta (disc_no);

update musicbrainz.acoustid_meta set album_artist=regexp_replace(t1.album_artist,'^[\t\n\r ]*','','g'); 
update musicbrainz.acoustid_meta set album=regexp_replace(t1.album,'^[\t\n\r ]*','','g'); 

-- Delete crappy data from acoyts_meta table
DROP TABLE musicbrainz.acoustid_incomplete_metadata;

CREATE TABLE musicbrainz.acoustid_incomplete_metadata 
(
    meta_id int
);

insert into musicbrainz.acoustid_incomplete_metadata select id from acoustid_meta where album_artist='Unknown Artist';
insert into musicbrainz.acoustid_incomplete_metadata select id from acoustid_meta where artist='Unknown Artist';
insert into musicbrainz.acoustid_incomplete_metadata select id from acoustid_meta where artist is null;
insert into musicbrainz.acoustid_incomplete_metadata select id from acoustid_meta where album='Unknown Title';
insert into musicbrainz.acoustid_incomplete_metadata select id from acoustid_meta where album is null;
insert into musicbrainz.acoustid_incomplete_metadata select id from acoustid_meta where track is null;

VACUUM ANALYZE musicbrainz.acoustid_incomplete_metadata; 

DELETE FROM musicbrainz.acoustid_meta where id in (select meta_id from acoustid_incomplete_metadata);
DELETE FROM musicbrainz.acoustid_track_meta where meta_id in (select meta_id from acoustid_incomplete_metadata);

VACUUM ANALYZE musicbrainz.acoustid_meta;
VACUUM ANALYZE musicbrainz.acoustid_track_meta;
