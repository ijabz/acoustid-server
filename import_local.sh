#!/bin/bash
export VERSION=$1
set -x
psql jthinksearch < CreateTables.sql
rm -fr import
mkdir import
cd import
s3cmd get s3://albunack/acoustid_track_fingerprint_$VERSION.csv.gz
s3cmd get s3://albunack/acoustid_track_$VERSION.csv.gz
s3cmd get s3://albunack/acoustid_meta_$VERSION.csv.gz 
s3cmd get s3://albunack/acoustid_track_meta_$VERSION.csv.gz
s3cmd get s3://albunack/acoustid_track_mbid_$VERSION.csv.gz
s3cmd get s3://albunack/acoustid_fingerprint_$VERSION.csv.gz
s3cmd get s3://albunack/acoustid_mbid_disabled_$VERSION.csv.gz
s3cmd get s3://albunack/acoustid_mbid_checked_$VERSION.csv.gz
gunzip *.gz
psql jthinksearch -c "COPY musicbrainz.acoustid_track_fingerprint FROM '/home/ubuntu/code/acoustid-server/import/acoustid_track_fingerprint_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_track FROM '/home/ubuntu/code/acoustid-server/import/acoustid_track_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_meta FROM '/home/ubuntu/code/acoustid-server/import/acoustid_meta_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_track_meta FROM '/home/ubuntu/code/acoustid-server/import/acoustid_track_meta_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_track_mbid FROM '/home/ubuntu/code/acoustid-server/import/acoustid_track_mbid_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_fingerprint FROM '/home/ubuntu/code/acoustid-server/import/acoustid_fingerprint_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_mbid_disabled FROM '/home/ubuntu/code/acoustid-server/import/acoustid_mbid_disabled_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_mbid_checked FROM '/home/ubuntu/code/acoustid-server/import/acoustid_mbid_checked_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch < ../CreatePrimaryKeys.sql
psql jthinksearch < ../CreateIndexes.sql
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track_mbid;";
psql jthinksearch -c "insert into acoustid_mbid(acoustid, mbid) select t1.gid,t2.mbid from acoustid_track t1 inner join acoustid_track_mbid t2 on t1.id=t2.track_id left join acoustid_mbid_disabled t3 on t1.gid=t3.acoustid and t2.mbid=t3.mbid where t2.disabled=false and t3.acoustid is null;"
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track_meta;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_meta;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track_fingerprint;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_mbid_disabled;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_mbid_checked;";
psql jthinksearch -c "SELECT setval('acoustid_mbid_checked_id_seq', COALESCE((SELECT MAX(id)+1 FROM acoustid_mbid_checked), 1), false);";
psql jthinksearch -c "SELECT setval('acoustid_mbid_disabled_id_seq', COALESCE((SELECT MAX(id)+1 FROM acoustid_mbid_disabled), 1), false);";
#rm *.csv
