#!/bin/bash
set -x
psql jthinksearch -c "drop table musicbrainz.acoustid_mbid";
psql jthinksearch -c "create table musicbrainz.acoustid_mbid ( id serial primary key, acoustid uuid not null, mbid uuid not null);"
psql jthinksearch -c "insert into musicbrainz.acoustid_mbid(acoustid, mbid) select distinct t1.gid,t2.mbid from musicbrainz.acoustid_track t1 inner join musicbrainz.acoustid_track_mbid t2 on t1.id=t2.track_id left join musicbrainz.acoustid_mbid_disabled t3 on t1.gid=t3.acoustid and t2.mbid=t3.mbid where (disabled=false or disabled is null) and t3.acoustid is null"
psql jthinksearch -c "create index acoustid_mbid_index on musicbrainz.acoustid_mbid (mbid);"
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_mbid;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track_mbid;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track_meta;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_meta;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track_fingerprint";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_fingerprint";
