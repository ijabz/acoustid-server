﻿-- derived table taht we use for creating the lucene index
DROP TABLE musicbrainz.acoustid_mbid;

CREATE TABLE musicbrainz.acoustid_mbid (
    id serial,
    acoustid uuid NOT NULL,
    mbid uuid NOT NULL
);

-- derived table taht we use for creating the lucene index
DROP TABLE musicbrainz.acoustid_mbid_disabled;

CREATE TABLE musicbrainz.acoustid_mbid_disabled (
    id serial,
    acoustid uuid NOT NULL,
    mbid uuid NOT NULL
);


-- derived table taht we use for creating the lucene index
DROP TABLE musicbrainz.acoustid_mbid_checked;

CREATE TABLE musicbrainz.acoustid_mbid_checked (
    id serial,
    acoustid uuid NOT NULL,
    mbid uuid NOT NULL
);

-- represents the Acoustid, gid is the acoustid
DROP TABLE musicbrainz.acoustid_track;

CREATE TABLE musicbrainz.acoustid_track (
    id int,
    created timestamp with time zone DEFAULT current_timestamp,
    gid uuid NOT NULL,
    new_id varchar(30)
);

DROP TABLE musicbrainz.acoustid_track_json;

CREATE TABLE musicbrainz.acoustid_track_json (  
    data jsonb
);

-- Mapping from Acoustid (track_id maps to id in acoustid_track. mbid is musicbrianz recoridng id) 
DROP TABLE musicbrainz.acoustid_track_mbid;

CREATE TABLE musicbrainz.acoustid_track_mbid (
    id int,
    track_id int NOT NULL,
    mbid uuid NOT NULL,
    created timestamp with time zone DEFAULT current_timestamp,
    submission_count int not null default 1,
    disabled boolean DEFAULT false
);


DROP TABLE musicbrainz.acoustid_track_mbid_json;

CREATE TABLE musicbrainz.acoustid_track_mbid_json (  
    data jsonb
);
-- Mapping between meta_id and track_id (is mbid here for denormalization ?)
DROP TABLE musicbrainz.acoustid_track_meta;

CREATE TABLE musicbrainz.acoustid_track_meta (
    id int ,
    track_id int NOT NULL,
    meta_id int NOT NULL,
    created timestamp with time zone DEFAULT current_timestamp,
    submission_count int not null default 1
);


DROP TABLE musicbrainz.acoustid_track_meta_json;

CREATE TABLE musicbrainz.acoustid_track_meta_json (  
    data jsonb
);
-- Meta info for this meta_id
DROP TABLE musicbrainz.acoustid_meta;
 
 
CREATE TABLE musicbrainz.acoustid_meta (
    id int ,
    track varchar,
    artist varchar,
    album varchar,
    album_artist varchar,
    track_no varchar,
    disc_no varchar,
    year varchar
);

DROP TABLE musicbrainz.acoustid_meta_json;

CREATE TABLE musicbrainz.acoustid_meta_json (  
    data jsonb
);

DROP TABLE musicbrainz.acoustid_fingerprint;
 
CREATE TABLE musicbrainz.acoustid_fingerprint (
    id int ,
    length int not null,
    created timestamp with time zone DEFAULT current_timestamp
);

DROP TABLE musicbrainz.acoustid_fingerprint_json;

CREATE TABLE musicbrainz.acoustid_fingerprint_json (  
    data jsonb
);

--Just need for the fullimport of fingerprint_track then gets copied into track_fingerprint
DROP TABLE musicbrainz.acoustid_fingerprint_track;
 
CREATE TABLE musicbrainz.acoustid_fingerprint_track (
    id serial,
    fingerprint_id int NOT NULL,
    track_id int NOT NULL,
    submission_count int not null default 1,
    created timestamp with time zone DEFAULT current_timestamp
);

DROP TABLE musicbrainz.acoustid_track_fingerprint;
 
CREATE TABLE musicbrainz.acoustid_track_fingerprint (
    id int,
    track_id int NOT NULL,
    fingerprint_id int NOT NULL,
    submission_count int not null default 1,
    created timestamp with time zone DEFAULT current_timestamp,
    updated timestamp with time zone DEFAULT current_timestamp
);

DROP TABLE musicbrainz.acoustid_track_fingerprint_json;

CREATE TABLE musicbrainz.acoustid_track_fingerprint_json (  
    data jsonb
);
