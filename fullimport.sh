#Use Acoustid data as part of albunack project

#Create Database tables and import latest full dump (2019)
./import.sh

#now import fingerprint delta files
./importfpdeltaall.sh

#Update database with jsonl daily updates (from 2020)
./updateall.sh 2020-02-01

#Rebuild mbid/acoustid table
./afterupdate.sh

#Build table for bad acoustid/mbids report
psql jthinksearch < calc_bad_acoustids1.sql
psql jthinksearch < calc_bad_acoustids2.sql
psql jthinksearch < calc_bad_acoustids3.sql
