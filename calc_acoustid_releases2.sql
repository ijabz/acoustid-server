
DROP TABLE musicbrainz.unique_artist;

CREATE TABLE musicbrainz.unique_artist
(
    artistid uuid primary key,
    name varchar
);

INSERT INTO musicbrainz.unique_artist
SELECT min(gid::varchar)::uuid, lower(name)
FROM  musicbrainz.artist
GROUP BY lower(name)
HAVING COUNT(*)=1
;

VACUUM ANALYZE musicbrainz.unique_artist;

--BEcause a meta_id can match to multiple tracks_ids, and a track_id can match to multiple mb_recording_ids we 
-- have to create atable just storing onre instance for a given meta_id
DROP TABLE musicbrainz.acoustid_matching_tracks_temp;

CREATE TABLE musicbrainz.acoustid_matching_tracks_temp
(
    meta_id int primary key,
    acoustid uuid,
    duration int,
    mbid uuid
);
 

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id < 5000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 5000000 and t1.meta_id < 10000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 10000000 and t1.meta_id < 15000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 15000000 and t1.meta_id < 20000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 20000000 and t1.meta_id < 25000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 25000000 and t1.meta_id < 30000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 30000000 and t1.meta_id < 35000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 35000000 and t1.meta_id < 40000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 40000000 and t1.meta_id < 45000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 45000000 and t1.meta_id < 50000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 50000000 and t1.meta_id < 55000000
order by t1.meta_id
;


INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 55000000 and t1.meta_id < 60000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 60000000 and t1.meta_id < 65000000
order by t1.meta_id
;

INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 65000000 and t1.meta_id < 70000000
order by t1.meta_id
;
INSERT INTO musicbrainz.acoustid_matching_tracks_temp(meta_id, acoustid, duration, mbid)
SELECT distinct on (t1.meta_id) t1.meta_id, t4.gid, t6.length, t7.mbid
FROM musicbrainz.acoustid_track_album t1
INNER JOIN musicbrainz.acoustid_complete_album t2
ON t1.album_id=t2.id
INNER JOIN musicbrainz.acoustid_track_meta t3
ON t1.meta_id=t3.meta_id
INNER JOIN musicbrainz.acoustid_track t4
ON t3.track_id=t4.id
LEFT JOIN musicbrainz.acoustid_track_fingerprint t5
ON t4.id=t5.track_id
LEFT JOIN musicbrainz.acoustid_fingerprint t6
ON t5.fingerprint_id=t6.id
LEFT JOIN musicbrainz.acoustid_mbid t7
ON t4.gid=t7.acoustid
where t1.meta_id >= 70000000
order by t1.meta_id
;

VACUUM ANALYZE musicbrainz.acoustid_matching_tracks_temp;

DROP TABLE musicbrainz.acoustid_matching_tracks;

CREATE TABLE musicbrainz.acoustid_matching_tracks
(
    meta_id int primary key,
    acoustid uuid,
    duration int,
    mbid uuid,
    artistid uuid
);

INSERT INTO musicbrainz.acoustid_matching_tracks(meta_id, acoustid, duration, mbid, artistid)
SELECT distinct on (meta_id) t1.*, t10.artistid
FROM musicbrainz.acoustid_matching_tracks_temp t1
INNER JOIN musicbrainz.acoustid_meta t3
ON t1.meta_id=t3.id
LEFT JOIN  musicbrainz.unique_artist t10
ON lower(t3.artist)=lower(t10.name)
;

-- Albums that seem to match existing musicbrainz albums but we dont have acoistids for the links
-- but we could add them in
DROP TABLE musicbrainz.acoustid_complete_matched_album;

CREATE TABLE musicbrainz.acoustid_complete_matched_album 
(
    id serial primary key,
    album_artist varchar,
    album varchar,
    track_total int,
    year varchar,
    musicbrainz_match int,
    musicbrainz_id int,
    musicbrainz_album_artist varchar,
    musicbrainz_album varchar
);

insert into musicbrainz.acoustid_complete_matched_album
select distinct on (album_artist, album) t1.*, t2.id, t3.name, t2.name
from musicbrainz.acoustid_complete_album t1
inner join  musicbrainz.release t2
on  
translate(regexp_replace(replace(regexp_replace(lower(t1.album),'[ ]*',''),' & ',' and '),'^(.*)\(.*?\)','\1'),'’‘ʻ“”″‐–‒—−','''''''"""-----')=
translate(regexp_replace(replace(regexp_replace(lower(t2.name) ,'[ ]*',''),' & ',' and '),'^(.*)\(.*?\)','\1'),'’‘ʻ“”″‐–‒—−','''''''"""-----')
inner join musicbrainz.artist_credit t3
on t2.artist_credit=t3.id
inner join musicbrainz.artist_credit_name t4
on t3.id=t4.artist_credit
inner join musicbrainz.artist t5
on t4.artist=t5.id
where 
(
translate(regexp_replace(replace(regexp_replace(lower(t1.album_artist),'[ ]*',''),' & ',' and '),'^(.*)\(.*?\)','\1'),'’‘ʻ“”″‐–‒—−','''''''"""-----')=
translate(regexp_replace(replace(regexp_replace(lower(t3.name),'[ ]*',''),' & ',' and ')        ,'^(.*)\(.*?\)','\1'),'’‘ʻ“”″‐–‒—−','''''''"""-----')
)
OR
(
translate(regexp_replace(replace(regexp_replace(lower(t1.album_artist),'[ ]*',''),' & ',' and '),'^(.*)\(.*?\)','\1'),'’‘ʻ“”″‐–‒—−','''''''"""-----')=
translate(regexp_replace(replace(regexp_replace(lower(t5.name),'[ ]*',''),' & ',' and ')        ,'^(.*)\(.*?\)','\1'),'’‘ʻ“”″‐–‒—−','''''''"""-----')
)
order by t1.album_artist, t1.album;
