#!/bin/bash
start_date=$(date -I -d "$1")   # Input in format yyyy-mm-dd
end_date=$(date -I)             # Today in format yyyy-mm-dd

echo "Start: $start_date"
echo "Today: $end_date"

d=$start_date                   #
end_d=$(date -d "$end_date" +%s) # End date in seconds

while [ $(date -d "$d" +%s) -le $end_d ]; do # Check dates in seconds
    echo ${d//-/ }               # Output the date but replace - with [space]
    ./update.sh ${d//-/ }
    d=$(date -I -d "$d + 1 day") # Next day
done
