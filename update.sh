#!/bin/sh
export YEAR=$1
export MONTH=$2
export DAY=$3
export LATEST=$YEAR-$MONTH-$DAY
echo "importing from $LATEST"
rm -fr $HOME/code/acoustid-server/update
mkdir $HOME/code/acoustid-server/update
cd $HOME/code/acoustid-server/update
wget https://data.acoustid.org/$YEAR/$YEAR-$MONTH/$LATEST-meta-update.jsonl.gz
wget https://data.acoustid.org/$YEAR/$YEAR-$MONTH/$LATEST-track-update.jsonl.gz
wget https://data.acoustid.org/$YEAR/$YEAR-$MONTH/$LATEST-track_mbid-update.jsonl.gz
wget https://data.acoustid.org/$YEAR/$YEAR-$MONTH/$LATEST-fingerprint-update.jsonl.gz
wget https://data.acoustid.org/$YEAR/$YEAR-$MONTH/$LATEST-track_fingerprint-update.jsonl.gz
wget https://data.acoustid.org/$YEAR/$YEAR-$MONTH/$LATEST-track_meta-update.jsonl.gz
gunzip *.gz

if test -f "$LATEST-fingerprint-update.jsonl"; then
   psql jthinksearch -c "copy musicbrainz.acoustid_fingerprint_json from '/home/ubuntu/code/acoustid-server/update/$LATEST-fingerprint-update.jsonl'";
   psql jthinksearch -c "insert into musicbrainz.acoustid_fingerprint select id, length, created from acoustid_fingerprint_json cross join jsonb_populate_record(null::musicbrainz.acoustid_fingerprint, data);"
   psql jthinksearch -c "truncate musicbrainz.acoustid_fingerprint_json;"; 
fi

if test -f "$LATEST-track_fingerprint-update.jsonl"; then
   psql jthinksearch -c "copy musicbrainz.acoustid_track_fingerprint_json from '/home/ubuntu/code/acoustid-server/update/$LATEST-track_fingerprint-update.jsonl'";
   psql jthinksearch -c "insert into musicbrainz.acoustid_track_fingerprint select id, track_id, fingerprint_id, submission_count, created, updated  from acoustid_track_fingerprint_json cross join jsonb_populate_record(null::musicbrainz.acoustid_track_fingerprint, data) where data::text not like '%updated%';"
   psql jthinksearch -c "delete from musicbrainz.acoustid_track_fingerprint where id in (select id from acoustid_track_fingerprint_json cross join jsonb_populate_record(null::musicbrainz.acoustid_track_fingerprint, data) where data::text like '%updated%');"
   psql jthinksearch -c "insert into musicbrainz.acoustid_track_fingerprint select id, track_id, fingerprint_id, submission_count  from acoustid_track_fingerprint_json cross join jsonb_populate_record(null::musicbrainz.acoustid_track_fingerprint, data) where data::text like '%updated%';"
   psql jthinksearch -c "truncate musicbrainz.acoustid_track_fingerprint_json;"; 
fi

if test -f "$LATEST-meta-update.jsonl"; then
   psql jthinksearch -c "copy musicbrainz.acoustid_meta_json from '/home/ubuntu/code/acoustid-server/update/$LATEST-meta-update.jsonl'";
   psql jthinksearch -c "insert into musicbrainz.acoustid_meta select id, track, artist, album_artist, track_no, disc_no, year from acoustid_meta_json cross join jsonb_populate_record(null::musicbrainz.acoustid_meta, data) where data::text not like '%updated%';"
   psql jthinksearch -c "delete from musicbrainz.acoustid_meta where id in (select id from acoustid_meta_json cross join jsonb_populate_record(null::musicbrainz.acoustid_meta, data) where data::text like '%updated%');"
   psql jthinksearch -c "insert into musicbrainz.acoustid_meta select id, track, artist, album_artist, track_no, disc_no, year from acoustid_meta_json cross join jsonb_populate_record(null::musicbrainz.acoustid_meta, data) where data::text like '%updated%';"
   psql jthinksearch -c "truncate musicbrainz.acoustid_meta_json;"; 
fi

if test -f "$LATEST-track-update.jsonl"; then
   psql jthinksearch -c "copy musicbrainz.acoustid_track_json from '/home/ubuntu/code/acoustid-server/update/$LATEST-track-update.jsonl'"
   #insert the new tracks only
   psql jthinksearch -c "insert into musicbrainz.acoustid_track select id, created, gid, new_id from acoustid_track_json cross join jsonb_populate_record(null::acoustid_track, data) where data::text not like '%updated%';"
   #update existing tracks when just have new ids fron existing acoustids
   psql jthinksearch -c "delete from musicbrainz.acoustid_track where id in (select id from acoustid_track_json cross join jsonb_populate_record(null::acoustid_track, data) where data::text like '%updated%');"
   psql jthinksearch -c "insert into musicbrainz.acoustid_track select id, created, gid, new_id from acoustid_track_json cross join jsonb_populate_record(null::acoustid_track, data) where data::text like '%updated%';"
   #empty temporary json table 
   psql jthinksearch -c "truncate musicbrainz.acoustid_track_json;"; 
fi

if test -f "$LATEST-track_mbid-update.jsonl"; then
   psql jthinksearch -c "copy musicbrainz.acoustid_track_mbid_json from '/home/ubuntu/code/acoustid-server/update/$LATEST-track_mbid-update.jsonl'";
   psql jthinksearch -c "insert into musicbrainz.acoustid_track_mbid select id, track_id, mbid, created, submission_count, disabled from acoustid_track_mbid_json cross join jsonb_populate_record(null::acoustid_track_mbid, data) where data::text not like '%updated%';"
   psql jthinksearch -c "delete from musicbrainz.acoustid_track_mbid where id in (select id from acoustid_track_mbid_json cross join jsonb_populate_record(null::acoustid_track_mbid, data) where data::text like '%updated%');"
   psql jthinksearch -c "insert into musicbrainz.acoustid_track_mbid select id, track_id, mbid, created, submission_count, disabled from acoustid_track_mbid_json cross join jsonb_populate_record(null::acoustid_track_mbid, data) where data::text like '%updated%';"
   psql jthinksearch -c "truncate musicbrainz.acoustid_track_mbid_json;"; 
fi

if test -f "$LATEST-track_meta-update.jsonl"; then
   psql jthinksearch -c "copy musicbrainz.acoustid_track_meta_json from '/home/ubuntu/code/acoustid-server/update/$LATEST-track_meta-update.jsonl'"
   #insert the new tracks only
   psql jthinksearch -c "insert into musicbrainz.acoustid_track_meta select id, track_id, meta_id, created,submission_count from acoustid_track_meta_json cross join jsonb_populate_record(null::acoustid_track_meta, data) where data::text not like '%updated%';"
   #update existing tracks when just have new ids fron existing acoustids
   psql jthinksearch -c "delete from musicbrainz.acoustid_track_meta where id in (select id from acoustid_track_meta_json cross join jsonb_populate_record(null::acoustid_track_meta, data) where data::text like '%updated%');"
   psql jthinksearch -c "insert into musicbrainz.acoustid_track_meta select id, track_id, meta_id, created,submission_count from acoustid_track_meta_json cross join jsonb_populate_record(null::acoustid_track_meta, data) where data::text like '%updated%';"
   #empty temporary json table
   psql jthinksearch -c "truncate musicbrainz.acoustid_track_meta_json;";
fi

cd ..
