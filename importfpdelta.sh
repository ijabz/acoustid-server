#!/bin/bash
set -x
export LATEST=$1
rm -fr fpdelta
mkdir -p fpdelta
cd fpdelta
wget --timeout 0 --tries 0 https://data.acoustid.org/fingerprint.delta.$LATEST.csv.gz
gunzip fingerprint.delta.$LATEST.csv.gz
cut -d\" -f2 --complement fingerprint.delta.$LATEST.csv | cut -d, -f2 --complement > fixed.csv
psql jthinksearch -c "copy musicbrainz.acoustid_fingerprint (id, length, created) from '/home/ubuntu/code/acoustid-server/fpdelta/fixed.csv' DELIMITER ',' CSV HEADER";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_fingerprint;";
