#!/bin/bash
set -x
export LATEST='2019-11-30'
mkdir fulldump
cd fulldump
#wget https://data.acoustid.org/track.full.$LATEST.csv.gz
#wget https://data.acoustid.org/meta.full.$LATEST.csv.gz
#wget https://data.acoustid.org/track_mbid.full.$LATEST.csv.gz
#wget https://data.acoustid.org/track_meta.full.$LATEST.csv.gz
#wget https://data.acoustid.org/fingerprint_track.full.$LATEST.csv.gz
#gunzip *.gz
psql jthinksearch < ../CreateTables.sql
psql jthinksearch -c "copy musicbrainz.acoustid_fingerprint_track (fingerprint_id, track_id, submission_count, created) from '/home/ubuntu/code/acoustid-server/fulldump/fingerprint_track.full.$LATEST.csv' DELIMITER ',' CSV HEADER";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_fingerprint_track;";
psql jthinksearch -c "insert into musicbrainz.acoustid_track_fingerprint (id, track_id, fingerprint_id, submission_count, created) select id,track_id, fingerprint_id, submission_count, created from musicbrainz.acoustid_fingerprint_track";
psql jthinksearch -c "copy musicbrainz.acoustid_track from '/home/ubuntu/code/acoustid-server/fulldump/track.full.$LATEST.csv' DELIMITER ',' CSV HEADER";
psql jthinksearch -c "copy musicbrainz.acoustid_meta from '/home/ubuntu/code/acoustid-server/fulldump/meta.full.$LATEST.csv' DELIMITER ',' CSV HEADER";
psql jthinksearch -c "copy musicbrainz.acoustid_track_mbid from '/home/ubuntu/code/acoustid-server/fulldump/track_mbid.full.$LATEST.csv' DELIMITER ',' CSV HEADER";
psql jthinksearch -c "copy musicbrainz.acoustid_track_meta from '/home/ubuntu/code/acoustid-server/fulldump/track_meta.full.$LATEST.csv' DELIMITER ',' CSV HEADER";
psql jthinksearch < ../CreatePrimaryKeys.sql
psql jthinksearch < ../CreateIndexes.sql
psql jthinksearch -c "insert into acoustid_mbid(acoustid, mbid) select t1.gid,t2.mbid from acoustid_track t1 inner join acoustid_track_mbid t2 on t1.id=t2.track_id left join acoustid_mbid_disabled t3 on t1.gid=t3.acoustid and t2.mbid=t3.mbid where t2.disabled=false and t3.acoustid is null;"
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track_mbid;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track_meta;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_meta;";
psql jthinksearch -c "VACUUM ANALYZE musicbrainz.acoustid_track_fingerprint;";
