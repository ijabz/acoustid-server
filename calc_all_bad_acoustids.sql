--original albunack report
CREATE EXTENSION fuzzystrmatch;

DROP TABLE musicbrainz.acoustid_names;


CREATE TABLE musicbrainz.acoustid_names (
    acoustid uuid NOT NULL,
    artistname varchar(64000),
    name varchar(64000),
    submission_count int
);

DROP TABLE musicbrainz.acoustid_multiplenames;

CREATE TABLE musicbrainz.acoustid_multiplenames (
    acoustid uuid NOT NULL,
    artistname varchar(64000),
    name varchar(64000),
    submission_count int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch (
    row_order serial not null,
    acoustid uuid NOT NULL,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    bad_submission_count int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int
);

DROP TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted;

CREATE TABLE musicbrainz.acoustid_invalidrecordingmatch_sorted (
    row_order serial not null,
    acoustid uuid NOT NULL,
    bad_artistname varchar(64000),
    bad_name varchar(64000),
    bad_submission_count int,
    good_artistname varchar(64000),
    good_name varchar(64000),
    good_submission_count int
);

--Create a list of acoutids, nked to multiple recoridng names after applying simplified musicbrainz recording name, submission count 
-- Split into three INSERTS because of timeout issue when run on my server
insert into musicbrainz.acoustid_names
select distinct t1.acoustid,t10.name, trim(regexp_replace(lower(t2.name),'^(.*)\(.*?\)','\1')), sum(t4.submission_count)
from acoustid_mbid t1
inner join musicbrainz.recording t2
on t1.mbid=t2.gid
inner join musicbrainz.artist_credit t10
on t2.artist_credit=t10.id
inner join acoustid_track t3
on t1.acoustid=t3.gid
inner join acoustid_track_mbid t4
on t1.mbid=t4.mbid
and t3.id=t4.track_id
where t2.id < 5000000
group by t1.acoustid, t10.name, trim(regexp_replace(lower(t2.name),'^(.*)\(.*?\)','\1'))
order by t1.acoustid;

insert into musicbrainz.acoustid_names
select distinct t1.acoustid,t10.name, trim(regexp_replace(lower(t2.name),'^(.*)\(.*?\)','\1')), sum(t4.submission_count)
from acoustid_mbid t1
inner join musicbrainz.recording t2
on t1.mbid=t2.gid
inner join musicbrainz.artist_credit t10
on t2.artist_credit=t10.id
inner join acoustid_track t3
on t1.acoustid=t3.gid
inner join acoustid_track_mbid t4
on t1.mbid=t4.mbid
and t3.id=t4.track_id
where t2.id >= 5000000
and t2.id =< 10000000
group by t1.acoustid, t10.name, trim(regexp_replace(lower(t2.name),'^(.*)\(.*?\)','\1'))
order by t1.acoustid;

insert into musicbrainz.acoustid_names
select distinct t1.acoustid,t10.name, trim(regexp_replace(lower(t2.name),'^(.*)\(.*?\)','\1')), sum(t4.submission_count)
from acoustid_mbid t1
inner join musicbrainz.recording t2
on t1.mbid=t2.gid
inner join musicbrainz.artist_credit t10
on t2.artist_credit=t10.id
inner join acoustid_track t3
on t1.acoustid=t3.gid
inner join acoustid_track_mbid t4
on t1.mbid=t4.mbid
and t3.id=t4.track_id
where t2.id > 10000000
group by t1.acoustid, t10.name, trim(regexp_replace(lower(t2.name),'^(.*)\(.*?\)','\1'))
order by t1.acoustid;

--Filter so only contains acoustids where they link to more than one recording with a different simplified name
--and after running soundex on the name should still be more than one name for the acoustid, i.e soundex doesnt
--resolves all the names for the acoustid to one value
insert into musicbrainz.acoustid_multiplenames
select t1.acoustid, t1.artistname, t1.name, sum(submission_count)
from musicbrainz.acoustid_names t1
inner join
(
select acoustid from musicbrainz.acoustid_names
group by acoustid
having count(*) >1
) t2
on t1.acoustid=t2.acoustid
inner join
(
select distinct acoustid from musicbrainz.acoustid_names
group by acoustid, soundex(name)
having count(*) =1
) t3
on t2.acoustid=t3.acoustid
group by t1.acoustid, t1.artistname, t1.name
order by t1.acoustid, t1.artistname,sum(submission_count);

-- Now for each acoustid trhat has multile mbnames linked to it we put the one with lowest source count
--into own table, but only if source is 1 or less and there ids a good match with a submissionc ount more than 1
insert into musicbrainz.acoustid_invalidrecordingmatch (acoustid, bad_artistname, bad_name, bad_submission_count, good_artistname, good_name, good_submission_count)
select distinct on (t1.acoustid)  t1.acoustid, t1.artistname, t1.name, t1.submission_count, t2.artistname, t2.name, t2.submission_count
from musicbrainz.acoustid_multiplenames t1
inner join musicbrainz.acoustid_multiplenames t2
on t1.acoustid=t2.acoustid
and t1.name!=t2.name
where t1.submission_count =1
and t2.submission_count > 1
order by t1.acoustid, t1.submission_count;

update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' & ',' and ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' & ',' and ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' two ',' 2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' two ',' 2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' three ',' 3 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' three ',' 3 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' four ',' 4 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' four ',' 4 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' five ',' 5 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' five  ',' 5 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' six ',' 6 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' six  ',' 6 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' seven ',' 7 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' seven ',' 7 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' eight ',' 8 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' eight ',' 8 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' nine ',' 9 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' nine ',' 9 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' ten ',' 10 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' ten ',' 10 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' eleven ',' 11 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' eleven ',' 11 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=REPLACE(bad_name,' twelve ',' 12 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=REPLACE(good_name,' twelve ',' 12 ');

update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(two )(.*)','2 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(two )(.*)','2 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(three )(.*)','3 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(three )(.*)','3 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(four )(.*)','4 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(four )(.*)','4 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(five )(.*)','5 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(five )(.*)','5 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(six )(.*)','6 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(six )(.*)','6 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(seven )(.*)','7 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(seven )(.*)','7 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(eight )(.*)','8 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(eight )(.*)','8 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(nine )(.*)','9 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(nine )(.*)','9 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(ten )(.*)','10 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(ten )(.*)','10 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(eleven )(.*)','11 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(eleven )(.*)','11 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set bad_name=regexp_replace(bad_name,'^(twelve )(.*)','12 \2 ');
update musicbrainz.acoustid_invalidrecordingmatch set good_name=regexp_replace(good_name,'^(twelve )(.*)','12 \2 ');

--Filter out matches where by the goodname is actually quite simailr to the badname
delete from musicbrainz.acoustid_invalidrecordingmatch t1
where t1.bad_name like t1.good_name || '%';
delete from musicbrainz.acoustid_invalidrecordingmatch t1
where t1.good_name like t1.bad_name || '%';
delete from musicbrainz.acoustid_invalidrecordingmatch t1
where t1.bad_name like '%' || t1.good_name;
delete from musicbrainz.acoustid_invalidrecordingmatch t1
where t1.good_name like '%' || t1.bad_name;
delete from musicbrainz.acoustid_invalidrecordingmatch where LEVENSHTEIN(substring(good_name,1,20),substring(bad_name,1,20)) < 4;

--Need another table because need sorted in right order for report which uses rowno field
insert into musicbrainz.acoustid_invalidrecordingmatch_sorted (acoustid, bad_artistname, bad_name, bad_submission_count, good_artistname, good_name, good_submission_count)
select t1.acoustid, t1.bad_artistname, t1.bad_name, t1.bad_submission_count, t1.good_artistname, t1.good_name, t1.good_submission_count
from musicbrainz.acoustid_invalidrecordingmatch t1
order by good_artistname, good_name;

-- Creates table that creates a filtered acoustid_mbid table that has potential bad matches removed, we can use is our jthinsearch index to prevent 
-- linking bad acoustids to musicbrainz releases when building index because will take some time for manul chckng of potential matches to occur
drop table musicbrainz.acoustid_mbid_filtered;

create table musicbrainz.acoustid_mbid_filtered 
( 
	id int primary key,
       	acoustid uuid not null, 
	mbid uuid not null
);

insert into musicbrainz.acoustid_mbid_filtered
select  t1.*
from musicbrainz.acoustid_mbid t1
inner join musicbrainz.recording t2
on t2.gid=t1.mbid
inner join  musicbrainz.artist_credit t10
on t2.artist_credit=t10.id
left join musicbrainz.acoustid_invalidrecordingmatch t3
on t1.acoustid = t3.acoustid
and trim(regexp_replace(lower(t2.name),'^(.*)\(.*?\)','\1'))=t3.bad_name
and t10.name=t3.bad_artistname
where t3.acoustid is null;

create index acoustid_mbid_filtered_index 
on musicbrainz.acoustid_mbid_filtered (mbid);

VACUUM ANALYZE musicbrainz.acoustid_mbid;
VACUUM ANALYZE musicbrainz.acoustid_mbid_filtered;
