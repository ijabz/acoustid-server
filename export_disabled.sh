#!/bin/bash
export VERSION="2021-12-02"
set -x
rm -fr export
mkdir export
cd export
psql jthinksearch -c "COPY musicbrainz.acoustid_mbid_disabled TO '/tmp/acoustid_mbid_disabled_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
psql jthinksearch -c "COPY musicbrainz.acoustid_mbid_checked TO '/tmp/acoustid_mbid_checked_$VERSION.csv' WITH (FORMAT CSV, HEADER)";
sudo mv /tmp/*.csv /home/ubuntu/code/acoustid-server/export
gzip *.csv
s3cmd put acoustid_mbid_disabled_$VERSION.csv.gz s3://albunack;
s3cmd put acoustid_mbid_checked_$VERSION.csv.gz s3://albunack;
