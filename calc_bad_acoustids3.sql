drop table musicbrainz.acoustid_mbid_filtered;

create table musicbrainz.acoustid_mbid_filtered 
( 
	id int primary key,
       	acoustid uuid not null, 
	mbid uuid not null
);

insert into musicbrainz.acoustid_mbid_filtered
select  t1.*
from musicbrainz.acoustid_mbid t1
left join musicbrainz.acoustid_mbid_disabled t2
on t1.acoustid     = t2.acoustid
and t1.mbid        = t2.mbid
left join musicbrainz.acoustid_invalidrecordingmatch t3
on t1.acoustid     = t3.acoustid
and t1.mbid        = t3.bad_mbid
left join musicbrainz.acoustid_invalidrecordingmatch_8 t4
on t1.acoustid     = t4.acoustid
and t1.mbid        = t4.bad_mbid
where t2.acoustid is null and t3.acoustid is null and t4.acoustid is null
;

create index acoustid_mbid_filtered_index 
on musicbrainz.acoustid_mbid_filtered (mbid);

VACUUM ANALYZE musicbrainz.acoustid_mbid;
VACUUM ANALYZE musicbrainz.acoustid_mbid_filtered;
