-- So we fix the data here so saved to our backup so dont have to do each time
update musicbrainz.acoustid_meta set album=null where album='\N';
update musicbrainz.acoustid_meta set album_artist=null where album_artist='\N';
update musicbrainz.acoustid_meta set track=null where track='\N';
update musicbrainz.acoustid_meta set track_no=null where track_no='\N';
update musicbrainz.acoustid_meta set disc_no=1 where disc_no='\N';
update musicbrainz.acoustid_meta set album_artist=artist where album_artist is null and artist is not null;
update musicbrainz.acoustid_meta set album_artist='Various Artists' where album_artist='Various';
update musicbrainz.acoustid_meta set album_artist='Various Artists' where album_artist='VARIOUS';
update musicbrainz.acoustid_meta set year=null where year='\N';

update musicbrainz.acoustid_meta set album_artist=regexp_replace(t1.album_artist,'^[\t\n\r ]*','','g'); 
update musicbrainz.acoustid_meta set album=regexp_replace(t1.album,'^[\t\n\r ]*','','g'); 

VACUUM ANALYZE musicbrainz.acoustid_meta;
